package atividade.segunda.bancodedados.restapi;

import atividade.segunda.bancodedados.dao.PessoaDAO;
import java.util.List;
import org.springframework.web.bind.annotation.*;
import atividade.segunda.bancodedados.domain.Pessoa;

@RestController
public class PessoaApi {
    
    private PessoaDAO pessoaDAO = new PessoaDAO();

    //listar todas as pessoas que estao no banco
    @GetMapping("/pessoas")
    public List<Pessoa> obterPessoas(){
        return pessoaDAO.findAll();
    }

    //trazer pessoa com o id informado
    private PessoaDAO pessoaID = new PessoaDAO();
    @RequestMapping(value="/id/{numero}", method= RequestMethod.GET)
    public Pessoa findById(@PathVariable(value = "numero") Integer numero) throws Exception{
        return (Pessoa) pessoaID.findByID(numero);
    }
}
