package atividade.segunda.bancodedados.domain;

import java.sql.Date;

public class Pessoa {

    private Integer id;
    private String nome;
    private Date data_nascimento;
    private String telefone;
    private String documento;

    public Integer getId() {
        return id;
    }
   
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData_nascimento() {
        if(data_nascimento == null) return null;
        return new Date(data_nascimento.getTime());
    }

    public void setData_nascimento(Date data_nascimento) {
        if(data_nascimento == null) this.data_nascimento = null;
        else this.data_nascimento = new Date(data_nascimento.getTime());
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "id=" + id + ",\n\t nome=" + nome + ",\n\t data_nascimento=" + data_nascimento + ",\n\t telefone=" + telefone + ",\n\t documento=" + documento + '}';
    }
    
    
}
